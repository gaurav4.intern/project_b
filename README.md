Assignment 11 - Git CMR Jira Cachet

Task-1:
Create git repo and host it on any public git hosting service [github / gitlab / bitbucket]
Clone that repo , create a branch, add a Readme file and push the changes to branch and get it merged by your mentor
( Verify if it has reflected on UI and attach screenshot as solution )


Task-2:
Simulate a merge conflict and try to resolve it.
( Share the steps as a solution)

Task-3:
Create 2 Git repositories, Project-A with Readme file and Project-B empty repository. Write a script to pull latest changes from Project-A/Readme file and push it to Project-B/Readme with proper commit message each time there is a change.
Additionally if you want the pull the latest changes from A to B periodically how would you do it (Explore various ways you can achieve this and implement anyone)
( Share the scripts as solution )

